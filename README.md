# Getting started Fivvy contextual-profiler
  
Contextual Profiler SDK offers a comprehensive and efficient solution for collecting valuable information about your users. With this powerful tool, you will be able to gather relevant data that will allow you to conduct in-depth analysis and gain a clear understanding of your users' behavior, preferences, and needs.


See the full [API](#api) for more methods.

## Recommendations
- ANDROID API LEVEL: 21 - 33
- MIN JAVA VERSION: jdk11 (we recommend jdk17)

## Installation
_Please read this entire section._

you MUST add this line to build.gradle (proyect)

```groovy

allprojects {
    repositories {
        google()
        mavenCentral()
          maven {
            url "https://gitlab.com/api/v4/projects/58175283/packages/maven"
        }
    }
}
```
### (Kotlin)
```groovy
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
        maven {
            url = uri("https://gitlab.com/api/v4/projects/58175283/packages/maven")
        }
    }
}
```
and in build.gradle (app)
```groovy
    implementation 'com.fivvy:fivvy-lib:1.1.1@aar'
```
### (Kotlin)
```groovy
    implementation("com.fivvy:fivvy-lib:1.1.1@aar")
```

make sure you also have:

```groovy
    implementation 'androidx.appcompat:appcompat:1.4.0'
    implementation 'androidx.work:work-runtime:2.8.0'
    implementation 'com.google.code.gson:gson:2.8.9'
    implementation 'com.squareup.okhttp3:okhttp:4.9.2'
```

### (Kotlin)
```groovy
    implementation ("androidx.appcompat:appcompat:1.4.0")
    implementation ("androidx.work:work-runtime:2.8.0")
    implementation ("com.google.code.gson:gson:2.8.9")
    implementation ("com.squareup.okhttp3:okhttp:4.9.2")
```

#### Permissions

### AndroidManifest 
Necesary to add this **xmlns:tools** insde the tag **manifest** on AndroidManifest inside *android/app/src/main* folder.
```xml
<manifest xmlns:tools="http://schemas.android.com/tools">
```

Need to add these permissions in the AndroidManifest inside *android/app/src/main* folder. 

```xml
<uses-permission  android:name="android.permission.INTERNET" />
<uses-permission  android:name="android.permission.PACKAGE_USAGE_STATS"  tools:ignore="ProtectedPermissions" />

<queries>
    <!-- List of package's [Max 100] -->
    <package android:name="com.whatsapp"/> <!-- WhatsApp Messenger -->
    <package android:name="com.facebook.katana"/> <!-- Facebook -->
    <package android:name="com.mercadopago.android"/> <!-- Mercado Pago -->
    <!-- ... -->

  </queries>


```
## Android Permission Request

On Android, you must request permissions beforehand to check the app's usage.

### Get User Permission to Check App Usage

We have a pre-made modal with instructions that can be used for this purpose. Here is an example of how to implement it:

First, if we want to display an icon from our app, we need to convert it to a byte array. For example (JAVA):

```java
  private byte[] convertDrawableToByteArray(Resources res, int drawableId) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        BitmapFactory.decodeResource(res, drawableId).compress(Bitmap.CompressFormat.PNG, 50, stream);
        return stream.toByteArray();
    }
```
(KOTLIN):
```kotlin
private fun convertDrawableToByteArray(res: Resources, drawableId: Int): ByteArray {
    val stream = ByteArrayOutputStream()
    BitmapFactory.decodeResource(res, drawableId).compress(Bitmap.CompressFormat.PNG, 50, stream)
    return stream.toByteArray()
}
```
Then, use te "UseCaseService"

(JAVA):
```java
  useCaseService = new UseCaseService(this); //init UseCaseService

  byte[] image = convertDrawableToByteArray(getResources(), R.drawable.yellow); // example

 useCaseService.openUsageAccessSettings(
      "EN", // If selected, the default texts will be set on the modal for EN, ES, or PR.
      // If using the ln value, it is recommended not to use the parameters below as the ln sets default texts in the right language.
      'Fivvy', // string value with your app name
      image,
      'Activate the permission', // optional description text. Recommended length: 3 or 4 words
      'Modal custom text', // optional modal text. Recommended length: 3 or 4 words
      "Dialog Title", // Title of the dialog displayed to the user before redirecting to the settings screen for permissions.
      "Dialog message 1", // Custom Message of the dialog displayed to the user before redirecting to the settings screen for permissions.
      "Dialog message 2" // Other custom Message of the dialog displayed to the user before redirecting to the settings screen for permissions.
    );
```

(KOTLIN):
```groovy
useCaseService = UseCaseService(this) //init UseCaseService

val image = convertDrawableToByteArray(resources, R.drawable.yellow) // example

useCaseService.openUsageAccessSettings(
    "EN", // If selected, the default texts will be set on the modal for EN, ES, or PR.
    "Fivvy", // string value with your app name
    image,
    "Activate the permission", // optional description text. Recommended length: 3 or 4 words
    "Modal custom text", // optional modal text. Recommended length: 3 or 4 words
    "Dialog Title", // Title of the dialog displayed to the user before redirecting to the settings screen for permissions.
    "Dialog message 1", // Custom Message of the dialog displayed to the user before redirecting to the settings screen for permissions.
    "Dialog message 2" // Other custom Message of the dialog displayed to the user before redirecting to the settings screen for permissions.
)
```



## Direct Access to Settings
If you prefer not to display any modal and want to go directly to the settings screen, you can use 
```java
useCaseService.openUsageAccessSettingsDirectly();
```

## Send data to Fivvy's analytics service
initContextualDataCollection
Like the useCaseService, this class should be initialized internally by passing the application context for internal reference.
This class publicly exposes the execute method, which collects contextual information from the user.

(JAVA)

```java
initContextDataCollection = new InitContextDataCollection(this);
```

(KOTLIN)

```kotlin
val initContextDataCollection = InitContextDataCollection(this)
```

Example in JAVA code for SDK implementation:
The button with the id button6 is assigned an event which calls the use case createSendContextualInformation. This method receives the parameters detailed in the previous table.
Although the example shows the use case in a button event, it could be done when loading a view, calling it in an onCreate.

Every time this code runs, the data will be send to the Fyvvy's API.

```java
      useCaseService.createSendContextualInformation(
      customerId, // Represents an identifier of the current user
      API_KEY, // ApiKey of Fivvy's API
      API_SECRET, // ApiSecrey of Fivvy's API
      DAYS, // Integer that represents the last days to recollect the app usage information of the user
      AUTH_API_URL, // URL of the Fivvy's Auth API 
      SEND_DATA_API_URL // URL of the Fivvy's Analytics Data API 
      );
```

## API
#### All the information about the package and how to use functions.

|   Methods	|  Params value 	|  Return value 	| Description  	|
|---	|---	|---	|---	|
|   `initContextualDataCollection`	|   (customerId: `String`, apiKey: `String`, apiSecret: `String`, appUsageDays: `Int`, authApiUrl: `String`, sendDataApiUrl: `String`)	|   `ContextualData` 	|   Initiates data collection, sending it to the Fivvy's Analytics Data API.	
|   `getDeviceInformation`	|   **Empty**	|   `Promise<IHardwareAttributes>`	|   Returns the device hardware information of the customer.	|
|   `getAppUsage`	|   `Int` days. Represent the last **days** to get the usage of each app.	|   `Promise<IAppUsage[]>` | Returns an **IAppUsage Array** for all the queries in AndroidManifest that user had install in his phone or  **null** if user doesn’t bring usage access.	|   Returns  **null** if the user doesnt brings access to the App Usage or an **IAppUsage Array** for the all used aplications.	|
|   `getAppsInstalled`	|   **Empty**	|   `Promise<IInstalledApps[]>`	|   Returns  an **IInstalledApps Array** for all the queries in AndroidManifest that user had install in his phone.	|
|   `openUsageAccessSettings`	|   (ln: String, appName: String, appDescription: String, modalText: String, imagePath: byte[], dialogTitle: String, dialogMessage1: String, dialogMessage2: String)	|   `Boolean`	|    Open settings view with pre-built modal to grant app usage permission.	|
|   `openUsageAccessSettingsDirectly`	|   **Empty**	|   `Boolean`	|    Open settings view without pre-built modal to grant app usage permission.	|


This is an example of an implementation on MainActivity.
(JAVA):
```java
public class MainActivity extends AppCompatActivity {
    private final Map<Integer, Runnable> buttonCommands = new HashMap<>();

    private Map<String, Object> contextualDataFromLibrary;

    private InitContextDataCollection initContextDataCollection;
    private UseCaseService useCaseService;
    static final String customerId = "ANDROID-VANILLA-DEMO-APP";

    //static
    private String contextualDataJson;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        useCaseService = new UseCaseService(this);
        setContentView(R.layout.activity_main);

        initializeViews();
        this.initContextDataCollection = new InitContextDataCollection(this);
        setContextualCollection();
        initializeButtonActions();

    }

    private void initializeViews() {
        findViewById(R.id.button).setOnClickListener(this::onClick);
       // findViewById(R.id.button3).setOnClickListener(this::onClick);

        findViewById(R.id.button3).setOnClickListener(this::onClick);
        findViewById(R.id.button4).setOnClickListener(this::onClick);
        findViewById(R.id.button5).setOnClickListener(this::onClick);
        findViewById(R.id.button6).setOnClickListener(this::onClick);
        findViewById(R.id.button7).setOnClickListener(this::onClick);

    }

    private void initializeButtonActions() {
        byte[] image = convertDrawableToByteArray(getResources(), R.drawable.yellow);

        buttonCommands.put(R.id.button, () -> {
                    String installedApps = useCaseService.createGetInstalledAppsUseCase().toString();
                    updateTextView(R.id.textView, installedApps);
                }
        );

        buttonCommands.put(R.id.button3, () -> updateTextView(R.id.textView1,  useCaseService.createGetAppUsageUseCase(30).toString()));
        buttonCommands.put(R.id.button4, () -> useCaseService.createOpenUsageSettingsDirectlyUseCase());
        buttonCommands.put(R.id.button7, () -> useCaseService.createOpenUsageSettingsUseCase("ES", "Fivvy app", "demo", image, null, "titulo", "descripcoin 1", "descripcion 2"));


        buttonCommands.put(R.id.button5, () -> updateTextView(R.id.textView3, useCaseService.createGetDeviceInfoUseCase().toString()));
        buttonCommands.put(R.id.button6, () -> {
                    useCaseService.createSendContextualInformation(
                            initContextDataCollection.execute(customerId,customerId,30),
                            "XXXXXXXXXXX",
                            "XXXXXXXXXXX",
                            "https://XXXXXXXXXXX",
                            "https://XXXXXXXXXXX");
                }
        );
    }
    private void onClick(View view) {
        Runnable command = buttonCommands.get(view.getId());
        if (command != null) {
            command.run();
        } else {
            // Manejar el caso en que no haya comando para este botón
        }
    }

    private void updateTextView(int id, String text) {
        ((TextView) findViewById(id)).setText(text);
    }

    private void setContextualCollection() {

        this.contextualDataFromLibrary = initContextDataCollection.execute(customerId,customerId,  30);

        Log.d("ContextualCollection:", "contextual data library "+this.contextualDataFromLibrary);
    }
    private byte[] convertDrawableToByteArray(Resources res, int drawableId) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        BitmapFactory.decodeResource(res, drawableId).compress(Bitmap.CompressFormat.PNG, 50, stream);
        return stream.toByteArray();
    }
}
```
(KOTLIN):
```kotlin
class MainActivity : ComponentActivity() {
    private val useCaseService: UseCaseService by lazy { UseCaseService(this) }
    private val initContextualDataCollection: InitContextDataCollection by lazy { InitContextDataCollection(this) }
    private var contextualDataFromLibrary: Map<String, Any>? = null

    companion object {
        const val customerId = "ANDROID-VANILLA-DEMO-APP"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContextualCollection()

        setContent {
            MyApplicationTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MainScreen(useCaseService, initContextualDataCollection)
                }
            }
        }
    }

    private fun setContextualCollection() {
        contextualDataFromLibrary = initContextualDataCollection.execute(customerId, customerId, 30)
        Log.d("ContextualCollection:", "contextual data library $contextualDataFromLibrary")
    }
}

@Composable
fun MainScreen(useCaseService: UseCaseService, initContextualDataCollection: InitContextDataCollection) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        Greeting("Android")

        Button(
            onClick = {
                useCaseService.createOpenUsageSettingsDirectlyUseCase()
            }
        ) {
            Text("Open Usage Settings Directly")
        }

        Button(
            onClick = {
                val image = ByteArray(0) // Replace with your image
                useCaseService.createOpenUsageSettingsUseCase(
                    "ES",
                    "Fivvy app",
                    "demo",
                    image,
                    null,
                    "titulo",
                    "descripcoin 1",
                    "descripcion 2"
                )
            }
        ) {
            Text("Open Usage Settings")
        }

        Button(
            onClick = {
                val installedApps = useCaseService.createGetInstalledAppsUseCase().toString()
                Log.d("InstalledApps", installedApps)
            }
        ) {
            Text("Get Installed Apps")
        }

        Button(
            onClick = {
                val appUsage = useCaseService.createGetAppUsageUseCase(30).toString()
                Log.d("AppUsage", appUsage)
            }
        ) {
            Text("Get App Usage")
        }

        Button(
            onClick = {
                val deviceInfo = useCaseService.createGetDeviceInfoUseCase().toString()
                Log.d("DeviceInfo", deviceInfo)
            }
        ) {
            Text("Get Device Info")
        }

        Button(
            onClick = {
                useCaseService.createSendContextualInformation(
                    initContextualDataCollection.execute(MainActivity.customerId, MainActivity.customerId, 30),
                    "XXXXXXXXXXX",
                    "XXXXXXXXXXX",
                    "https://XXXXXXXXXXX",
                    "https://XXXXXXXXXXX"
                )
            }
        ) {
            Text("Send Contextual Information")
        }
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    MyApplicationTheme {
        MainScreen(useCaseService = UseCaseService(null), initContextualDataCollection = InitContextDataCollection(null))
    }
}
```



## Terms of use
All content here is the property of Fivvy, it should not be used without their permission.






