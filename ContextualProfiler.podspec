Pod::Spec.new do |s|
  s.name         = "ContextualProfiler"
  s.version      = "0.1.0"
  s.summary      = "Librería para detectar apps instaladas usando canOpenURL."
  s.homepage     = "https://fivvy.com"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "Usuario" => "usuario@example.com" }
  s.source       = { :http => "https://gitlab.com/ffragueiro/fivvy-contextual-profiler-maven/-/raw/main/ContextualProfiler.xcframework.zip" }
  s.ios.deployment_target = "12.0"
  s.source_files = "*.{swift}"
end
